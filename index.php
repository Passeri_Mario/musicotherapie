<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Voici les routes pour enregistrer des itinéraires Web de l'application.
| Les routes sont chargées par $_SERVER['REQUEST_URI']
|
*/

if($_SERVER['HTTPS'] == 'on'){} else{header('Location: https://musicotherapie.mariopasseri.eu');}

$request = $_SERVER['REQUEST_URI'];

switch ($request) {

    // Page de 1er niveau

    case '/' :
    case '/index':
    case '/musicotherapie':
        require __DIR__ . '/view/home.php';
        break;

    case '/espace' :
        require __DIR__ . '/view/espace.php';
        break;

    case '/comptecree' :
        require __DIR__ . '/view/comptecree.php';
        break;

    case '/compositeurs' :
        require __DIR__ . '/view/compositeurs.php';
        break;

    case '/installation' :
        require __DIR__ . '/view/installation.php';
        break;

    default:
        case '.php';
        http_response_code(404);
        require __DIR__ . '/view/home.php';
        break;

    // Catégorie Douce Nuit

    case '/douce_nuit' :
        require __DIR__ . '/view/douce_nuit.php';
        break;

    case '/lecteur_douce_nuit' :
        require __DIR__ . '/view/lecteur_douce_nuit.php';
        break;

    // Categorie Relaxante

    case '/relaxante' :
        require __DIR__ . '/view/relaxante.php';
        break;

    case '/lecteur_relaxante' :
        require __DIR__ . '/view/lecteur_relaxante.php';
        break;

    // Categorie Nature

    case '/nature' :
        require __DIR__ . '/view/nature.php';
        break;

    case '/lecteur_nature' :
        require __DIR__ . '/view/lecteur_nature.php';
        break;

    // Categorie Météorologique

    case '/meteorologique' :
        require __DIR__ . '/view/meteorologique.php';
        break;

    case '/lecteur_meteorologique' :
        require __DIR__ . '/view/lecteur_meteorologique.php';
        break;

    // Categorie Énergisante

    case '/energisante' :
        require __DIR__ . '/view/energisante.php';
        break;

    case '/lecteur_energisante' :
        require __DIR__ . '/view/lecteur_energisante.php';
        break;

    // Categorie Aventure

    case '/aventure' :
        require __DIR__ . '/view/aventure.php';
        break;

    case '/lecteur_aventure' :
        require __DIR__ . '/view/lecteur_aventure.php';
        break;
}