'use strict';

function Formulaire() {

    if( document.getElementById("Pseudo").placeholder != null){ // Si y a des infos sur le compte
        document.getElementById("Pseudo").placeholder = localStorage.getItem("Pseudo");
        document.getElementById("Age").placeholder = localStorage.getItem("Age") + " ans";
    }

    if (localStorage.getItem("Pseudo") == null) { // Pas d'infos sur le compte
        document.getElementById("Pseudo").placeholder = ("Pseudo");
        document.getElementById("Age").placeholder = ("Age");
        document.getElementById("CompteValide").style.display ="none";
    }

    if (localStorage.getItem("Pseudo")) { // Bloc la modification du input Age si déja créer
        document.getElementById("Pseudo").readOnly = true;
    }

    if (localStorage.getItem("Age")) { // Bloc la modification du input Age si déja créer
        document.getElementById("Age").readOnly = true;
        document.getElementById("ButtonSubmit").style.display = "none";
        document.getElementById("CompteValide").textContent = "Votre compte et validé";
    }

    if(localStorage.getItem("Pseudo")){
        document.getElementById('ButtonSubmit').style.display='none';
    }
    else {
        document.getElementById('suppr').style.display='none';
    }
}

function TraitementFormulaire() { // Gestion du formulaire en LocalStorage

    let Pseudo = document.getElementById('Pseudo').value; // Le pseudo
    let Age = document.getElementById('Age').value; // l'adresse email

    localStorage.setItem("Pseudo", Pseudo); // Stockage du pseudo
    if (Age == "") {
    }
    else {
        localStorage.setItem("Age", Age); // Stockage de l'age
    }
}

// Fonction pour les input qui oblige uniquement des chiffres
function Verif_nombre(champ) {

    let chiffres = new RegExp("[0-9,]");
    let verif;
    let points = 0;

    for(let x = 0; x < champ.value.length; x++) {
        verif = chiffres.test(champ.value.charAt(x));
        if(champ.value.charAt(x) === "."){points++;}
        if(points > 1){verif = false; points = 1;}
        if(verif === false){champ.value = champ.value.substr(0,x) + champ.value.substr(x+1,champ.value.length-x+1); x--;}
    }
}
