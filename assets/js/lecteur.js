let audio = new Audio('song.mp3'); audio.preload = "auto"; // Déclaration de la musique

function Load() {audio.preload = "auto";audio.pause();audio.currentTime = 0;} // Scan au démarrage pour afficher la durée de la musique

addEventListener("load", Load(), false);

(function($) {

    $(document).ready(function() {

        // Déclaration de la musique

        // Jouer la musique
        let playButton = $('.control-play');playButton.on('click', function() {$(".vinyl").addClass("vinyl-fonction"); // Faire tourner le disqueaudio.play();
            audio.play();
            audio.loop = false;
        });

        // Pause de la lecture
        let playButton2 = $('.control-pause');playButton2.on('click', function() {
            $(".vinyl").removeClass("vinyl-fonction");  // Stoper le disque
            // simple méthode pour faire pause
            audio.pause();
        });

        // Arrêt de la lecture et retour au début de la musique
        let playButton3 = $('.control-stop');playButton3.on('click', function() {$(".vinyl").removeClass("vinyl-fonction"); // Stoper le disque
            // pause + audio.currentTime
            audio.pause();
            audio.currentTime = 0; // Destruction de la musique en cours
        });

        // Bouton pour répéter la musique
        let playButton4 = $('.control-boucle');playButton4.on('click', function() {
            console.log('Bouclage activé !!');
            audio.loop = true;
        });

        let  seekslider, seeking=false, seekto, curtimetext, durtimetext;

        seekslider = document.getElementById("seekslider");
        curtimetext = document.getElementById("curtimetext");
        durtimetext = document.getElementById("durtimetext");

        // On écoute les évènements du lecteur

        seekslider.addEventListener("mousedown", function(event){ seeking=true; seek(event); });

        seekslider.addEventListener("mousemove", function(event){ seek(event); });

        seekslider.addEventListener("mouseup",function(){ seeking=false; });

        audio.addEventListener("timeupdate", function(){ seektimeupdate(); });

        // Mes Functions

        function seek(event){
            if(seeking){
                seekslider.value = event.clientX - seekslider.offsetLeft;
                seekto = audio.duration * (seekslider.value / 100);
                audio.currentTime = seekto;
            }
        }

        function seektimeupdate(){
            let Times = audio.currentTime * (100 / audio.duration);
            seekslider.value = Times;

            let curmins = Math.floor(audio.currentTime / 60);
            let cursecs = Math.floor(audio.currentTime - curmins * 60);
            let durmins = Math.floor(audio.duration / 60);
            let dursecs = Math.floor(audio.duration - durmins * 60);

            if(cursecs < 10){ cursecs = "0"+cursecs; }
            if(dursecs < 10){ dursecs = "0"+dursecs; }
            if(curmins < 10){ curmins = "0"+curmins; }
            if(durmins < 10){ durmins = "0"+durmins; }

            // valeur des deux div qui affiche temps

            curtimetext.innerHTML = curmins+":"+cursecs;
            durtimetext.innerHTML = durmins+":"+dursecs;

        }

    });

})(jQuery);

localStorage.setItem("Categorie ", "Musique ");
