'use strict';

M.AutoInit(); // Init dev framework

function Membre(){
    if (localStorage.getItem("Pseudo")) {
        document.getElementById("Pseudos").innerText = "Bonjour " + " " + localStorage.getItem("Pseudo");
        document.getElementById("PseudoMobile").textContent = "Bonjour " + " " + localStorage.getItem("Pseudo"); // Pseudo Mobile
    }
    else {
        document.getElementById("Pseudos").innerText = "";
        document.getElementById("PseudoMobile").innerText = ""; // Pas de Pseudo Mobile
    }
}

addEventListener("load", Membre(), false);

// Detection du local de sauvegarde

function SuppresionDesDonnees() { // Fonction qui permet à l'utilisateur de tous suprimmer c'est donnée Local
    document.cookie = 'AcceptApps=AcceptApps; expires=Sun, 01 Feb 2000 00:00:00 UTC; path=/';
    localStorage.clear(); // Suppression de tout le LocalStorage
    window.location.href = "/"; // Retour
}

if (typeof(Storage) !== "undefined") { // detection si LocalStorage fonctionne
    console.log('LocaleStorage en activé')

} else {
    console.log('LocaleStorage n\'est pas activé ERREUR !! ');
}

function CompteCree() {

    if(localStorage.getItem("Pseudo")){
        document.location.href="/";
    }

}

