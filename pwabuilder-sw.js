
// Il s'agit de l'opérateur de service avec le réseau Cache-first

const CACHE = "pwabuilder-precache";
const precacheFiles = [

    /* Ajoute un tableau de fichiers à précacheter pour votre application */
];

self.addEventListener("install", function (event) {
    console.log("[PWA Builder] Install Event processing");

    console.log("[PWA Builder] Skip waiting on install");
    self.skipWaiting();

    event.waitUntil(
        caches.open(CACHE).then(function (cache) {
            console.log("[PWA Builder] Caching pages during install");
            return cache.addAll(precacheFiles);
        })
    );
});


// Permettre à sw de contrôler la page en cours
self.addEventListener("activate", function (event) {
    console.log("[PWA Builder] Claiming clients for current page");
    event.waitUntil(self.clients.claim());
});


// En cas d'échec de l'extraction, la requête dans le cache est recherchée et notifiée à partir de là.
self.addEventListener("fetch", function (event) {
    if (event.request.method !== "GET") return;

    event.respondWith(
        fromCache(event.request).then(
            function (response) {
                // La réponse a été trouvée dans le cache, nous y répondons et mettons à jour l'entrée
                // C’est là que nous appelons le serveur pour obtenir la dernière version du

                // fichier à utiliser lors de la prochaine visualisation
                event.waitUntil(
                    fetch(event.request).then(function (response) {
                        return updateCache(event.request, response);
                    })
                );

                return response;
            },
            function () {

                // La réponse n'a pas été trouvée dans le cache, je la cherchons sur le serveur.
                return fetch(event.request)
                    .then(function (response) {

                        // Si la requête aboutit, ajoutez-la ou mettez-la à jour dans le cache.
                        event.waitUntil(updateCache(event.request, response.clone()));

                        return response;
                    })
                    .catch(function (error) {
                        console.log("[PWA Builder] Network request failed and no cache." + error);
                    });
            }
        )
    );
});

function fromCache(request) {
    // Vérifie si tu l'as dans la cache
    // retourne la réponse
    // Si pas dans le cache, alors retourne
    return caches.open(CACHE).then(function (cache) {
        return cache.match(request).then(function (matching) {
            if (!matching || matching.status === 404) {
                return Promise.reject("no-match");
            }

            return matching;
        });
    });
}

function updateCache(request, response) {
    return caches.open(CACHE).then(function (cache) {
        return cache.put(request, response);
    });
}



