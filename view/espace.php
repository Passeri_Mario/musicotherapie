<!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>Musicothérapie</title>
        <meta property="og:title" content="Application PWA Musicothérapie" />
        <meta property="og:url" content="https://musicotherapie.mariopasseri.eu" />
        <meta name="geo.placename" content="Soultz-Sous-Forêts, Alsace">
        <meta name="description" content="Application mobile PWA de Musicothérapie comportant différentes catégories de musiques afin d'accompagner
                 l'utilisateur dans son quotidien."/>
        <meta name="author" content="Passeri Mario" />
        <meta name="copyright" content="Passeri Mario" />
        <meta name="geo.country" content="FR">
        <!-- PWA -->
        <link rel="manifest" href="../manifest.json">
        <meta name="theme-color" content="#3c4ca6"/>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../assets/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../assets/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../assets/img/favicon/android-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../assets/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="../assets/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
        <!-- css -->
        <link rel="stylesheet" href="../assets/css/MaterialICO.css" >
        <link rel="stylesheet" href="../assets/css/materialize.css" >
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>

    <body onload="Formulaire(); Membre();">

    <?php include('view/header.php'); ?>

    <main>
        <div class="wrapper">
            <div id="form" class="form">
                <form method="post" action="/comptecree">
                    <div class="input-container">
                            <img src="../assets/img/logo/music-page1.svg" class="avatar"/>
                            <br>
                            <h2>INFORMATIONS</h2>
                            <p>Gérer vos informations liées à l'application</p>
                            <br>
                            <p id="CompteValide"></p>
                            <br>
                            <input id="Pseudo" minlength="2" maxlength="15" name="pseudo" type="text" placeholder="" required="required" />
                            <br>
                            <input onkeyup="Verif_nombre(this);" id="Age" minlength="2" maxlength="2" name="age" type="text" placeholder="" required="required" />
                    </div>
                    <button id="ButtonSubmit" onclick="TraitementFormulaire()" class="btn-compte btn" name="envoi" type="submit">Enregistrer</button>
                    <button id="suppr" class="btn-compte btn wrapper waves-effect waves-light modal-trigger" href="#modal1">Éffacer toutes mes données</button>
                </form>
            </div>
            <div class="wrapper container">
                <a href="../assets/doc/License.pdf"><button class="btn-compte btn">License</button></a>
            </div>
        </div>

        <div id="modal1" class="modal">
            <div class="modal-content">
                <h4 class="center">Suppression</h4>
                <p style="color: black">Toutes vos données seront complétement supprimer.</p>
                <div style="display: flex; justify-content: space-between">
                    <a onclick="SuppresionDesDonnees()" href="/" class="z-depth-5 center modal-close waves-effect waves-green btn">Supprimer</a>
                    <a href="/" class="z-depth-5 center modal-close waves-effect waves-green btn">Retour</a>
                </div>
            </div>
        </div>
    </main>

    <script src="../assets/js/compte.js"></script>

    <?php include ('view/footer.php'); ?>

    </body>
</html>
