<?php if (empty($_POST['song'])){ header('Location: /');}?>

<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
            <title>Musicothérapie</title>
            <meta property="og:title" content="Application PWA Musicothérapie" />
            <meta property="og:url" content="https://musicotherapie.mariopasseri.eu" />
            <meta name="geo.placename" content="Soultz-Sous-Forêts, Alsace">
            <meta name="description" content="Application mobile PWA de Musicothérapie comportant différentes catégories de musiques afin d'accompagner
                     l'utilisateur dans son quotidien."/>
            <meta name="author" content="Passeri Mario" />
            <meta name="copyright" content="Passeri Mario" />
            <meta name="geo.country" content="FR">
            <!-- PWA -->
            <link rel="manifest" href="../manifest.json">
            <meta name="theme-color" content="#3c4ca6"/>
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="mobile-web-app-capable" content="yes">
            <!-- Favicon -->
            <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="../assets/img/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="../assets/img/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="../assets/img/favicon/android-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="../assets/img/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192" href="../assets/img/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
            <!-- css -->
            <link href="../assets/css/MaterialICO.css" rel="stylesheet">
            <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
            <link href="../assets/css/lecteurs/meteo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        </head>

    <body>

    <?php include ('view/header.php'); ?>

    <script>
        let audio = new Audio('assets/mp3/Meteo/<?php echo htmlspecialchars($_POST['song']); ?>.mp3'); // Déclaration de la musique
        window.onload = function(){
        function Load() {audio.preload = "auto";audio.pause();audio.currentTime = 0;}
        addEventListener("load", Load(), false);
        let _0x8a0d=["\x2E\x63\x6F\x6E\x74\x72\x6F\x6C\x2D\x70\x6C\x61\x79","\x63\x6C\x69\x63\x6B","\x76\x69\x6E\x79\x6C\x2D\x66\x6F\x6E\x63\x74\x69\x6F\x6E","\x61\x64\x64\x43\x6C\x61\x73\x73","\x2E\x76\x69\x6E\x79\x6C","\x70\x6C\x61\x79","\x6C\x6F\x6F\x70","\x6F\x6E","\x2E\x63\x6F\x6E\x74\x72\x6F\x6C\x2D\x70\x61\x75\x73\x65","\x72\x65\x6D\x6F\x76\x65\x43\x6C\x61\x73\x73","\x70\x61\x75\x73\x65","\x2E\x63\x6F\x6E\x74\x72\x6F\x6C\x2D\x73\x74\x6F\x70","\x63\x75\x72\x72\x65\x6E\x74\x54\x69\x6D\x65","\x2E\x63\x6F\x6E\x74\x72\x6F\x6C\x2D\x62\x6F\x75\x63\x6C\x65","\x42\x6F\x75\x63\x6C\x61\x67\x65\x20\x61\x63\x74\x69\x76\xE9\x20\x21\x21","\x6C\x6F\x67","\x73\x65\x65\x6B\x73\x6C\x69\x64\x65\x72","\x67\x65\x74\x45\x6C\x65\x6D\x65\x6E\x74\x42\x79\x49\x64","\x63\x75\x72\x74\x69\x6D\x65\x74\x65\x78\x74","\x64\x75\x72\x74\x69\x6D\x65\x74\x65\x78\x74","\x6D\x6F\x75\x73\x65\x64\x6F\x77\x6E","\x61\x64\x64\x45\x76\x65\x6E\x74\x4C\x69\x73\x74\x65\x6E\x65\x72","\x6D\x6F\x75\x73\x65\x6D\x6F\x76\x65","\x6D\x6F\x75\x73\x65\x75\x70","\x74\x69\x6D\x65\x75\x70\x64\x61\x74\x65","\x76\x61\x6C\x75\x65","\x63\x6C\x69\x65\x6E\x74\x58","\x6F\x66\x66\x73\x65\x74\x4C\x65\x66\x74","\x64\x75\x72\x61\x74\x69\x6F\x6E","\x66\x6C\x6F\x6F\x72","\x30","\x69\x6E\x6E\x65\x72\x48\x54\x4D\x4C","\x3A","\x72\x65\x61\x64\x79"];(function(_0xf2e0x1){_0xf2e0x1(document)[_0x8a0d[33]](function(){var _0xf2e0x2=_0xf2e0x1(_0x8a0d[0]);_0xf2e0x2[_0x8a0d[7]](_0x8a0d[1],function(){_0xf2e0x1(_0x8a0d[4])[_0x8a0d[3]](_0x8a0d[2]);audio[_0x8a0d[5]]();audio[_0x8a0d[6]]= false});var _0xf2e0x3=_0xf2e0x1(_0x8a0d[8]);_0xf2e0x3[_0x8a0d[7]](_0x8a0d[1],function(){_0xf2e0x1(_0x8a0d[4])[_0x8a0d[9]](_0x8a0d[2]);audio[_0x8a0d[10]]()});var _0xf2e0x4=_0xf2e0x1(_0x8a0d[11]);_0xf2e0x4[_0x8a0d[7]](_0x8a0d[1],function(){_0xf2e0x1(_0x8a0d[4])[_0x8a0d[9]](_0x8a0d[2]);audio[_0x8a0d[10]]();audio[_0x8a0d[12]]= 0});var _0xf2e0x5=_0xf2e0x1(_0x8a0d[13]);_0xf2e0x5[_0x8a0d[7]](_0x8a0d[1],function(){console[_0x8a0d[15]](_0x8a0d[14]);audio[_0x8a0d[6]]= true});var _0xf2e0x6,_0xf2e0x7=false,_0xf2e0x8,_0xf2e0x9,_0xf2e0xa;_0xf2e0x6= document[_0x8a0d[17]](_0x8a0d[16]);_0xf2e0x9= document[_0x8a0d[17]](_0x8a0d[18]);_0xf2e0xa= document[_0x8a0d[17]](_0x8a0d[19]);_0xf2e0x6[_0x8a0d[21]](_0x8a0d[20],function(_0xf2e0xb){_0xf2e0x7= true;_0xf2e0xc(_0xf2e0xb)});_0xf2e0x6[_0x8a0d[21]](_0x8a0d[22],function(_0xf2e0xb){_0xf2e0xc(_0xf2e0xb)});_0xf2e0x6[_0x8a0d[21]](_0x8a0d[23],function(){_0xf2e0x7= false});audio[_0x8a0d[21]](_0x8a0d[24],function(){_0xf2e0xd()});function _0xf2e0xc(_0xf2e0xb){if(_0xf2e0x7){_0xf2e0x6[_0x8a0d[25]]= _0xf2e0xb[_0x8a0d[26]]- _0xf2e0x6[_0x8a0d[27]];_0xf2e0x8= audio[_0x8a0d[28]]* (_0xf2e0x6[_0x8a0d[25]]/ 100);audio[_0x8a0d[12]]= _0xf2e0x8}}function _0xf2e0xd(){var _0xf2e0xe=audio[_0x8a0d[12]]* (100/ audio[_0x8a0d[28]]);_0xf2e0x6[_0x8a0d[25]]= _0xf2e0xe;var _0xf2e0xf=Math[_0x8a0d[29]](audio[_0x8a0d[12]]/ 60);var _0xf2e0x10=Math[_0x8a0d[29]](audio[_0x8a0d[12]]- _0xf2e0xf* 60);var _0xf2e0x11=Math[_0x8a0d[29]](audio[_0x8a0d[28]]/ 60);var _0xf2e0x12=Math[_0x8a0d[29]](audio[_0x8a0d[28]]- _0xf2e0x11* 60);if(_0xf2e0x10< 10){_0xf2e0x10= _0x8a0d[30]+ _0xf2e0x10};if(_0xf2e0x12< 10){_0xf2e0x12= _0x8a0d[30]+ _0xf2e0x12};if(_0xf2e0xf< 10){_0xf2e0xf= _0x8a0d[30]+ _0xf2e0xf};if(_0xf2e0x11< 10){_0xf2e0x11= _0x8a0d[30]+ _0xf2e0x11};_0xf2e0x9[_0x8a0d[31]]= _0xf2e0xf+ _0x8a0d[32]+ _0xf2e0x10;_0xf2e0xa[_0x8a0d[31]]= _0xf2e0x11+ _0x8a0d[32]+ _0xf2e0x12}})})(jQuery);
        localStorage.setItem("Categorie Meteorologique<?php echo htmlspecialchars($_POST['song']); ?>", "Musique <?php echo htmlspecialchars($_POST['song']); ?>");
        }
    </script>

    <main>
        <div class="Lecteur">
            <div class="music-player">
                <div class="player-content-container">
                    <div class="Description-musique">
                        <!-- On ajoute le numéro de la musique pour le titre -->
                        <p><?php echo htmlspecialchars(ucwords(str_replace('meteo', 'météo ', $_POST['song'])))?></p>
                        <div class="music-player-controls">
                            <a href="/meteorologique"><i style="background-color: #18a599; color: white; border-radius: 50%; font-size: 40px;" class="material-icons">arrow_back</i></a>
                        </div>
                    </div>

                    <div class="vinyl record">
                        <div class='heart'></div>
                    </div>

                    <div class="music-player-controls">

                        <div class="btn control-play"><i class="material-icons">play_arrow</i></div><!-- Marche  -->
                        <div class="btn control-pause"><i class="material-icons">pause</i></div><!-- Pause  -->
                        <div class="btn control-stop"><i class="material-icons">stop</i></div><!-- Arrêt  -->
                        <div class="btn control-boucle"><i class="material-icons">autorenew</i></div><!-- Boucle  -->

                        <button id="playpausebtn"></button>
                        <button id="mutebtn"></button>
                    </div>

                    <div style="display: flex; justify-content: center; margin-top: 70px">
                        <input id="volumeslider" type="hidden" min="0" max="100" value="100" step="1" title="Volume">
                        <input id="seekslider" type="hidden" min="0" max="100" value="0" step="1" title="Temps">

                        <div id="timebox">
                            <span id="curtimetext">00:00</span> / <span id="durtimetext">00:00</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include ('view/footer.php'); ?>

    </body>
</html>
