        <!DOCTYPE html>
        <html lang="fr">
        <head>
            <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
            <title>Musicothérapie</title>
            <meta property="og:title" content="Application PWA Musicothérapie" />
            <meta property="og:url" content="https://musicotherapie.mariopasseri.eu" />
            <meta name="geo.placename" content="Soultz-Sous-Forêts, Alsace">
            <meta name="description" content="Application mobile PWA de Musicothérapie comportant différentes catégories de musiques afin d'accompagner
                     l'utilisateur dans son quotidien."/>
            <meta name="author" content="Passeri Mario" />
            <meta name="copyright" content="Passeri Mario" />
            <meta name="geo.country" content="FR">
            <!-- PWA -->
            <link rel="manifest" href="../manifest.json">
            <meta name="theme-color" content="#3c4ca6"/>
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="mobile-web-app-capable" content="yes">
            <!-- Favicon -->
            <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="../assets/img/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="../assets/img/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="../assets/img/favicon/android-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="../assets/img/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192" href="../assets/img/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
            <!-- css -->
            <link rel="stylesheet" href="../assets/css/MaterialICO.css" >
            <link rel="stylesheet" href="../assets/css/materialize.css" >
            <link rel="stylesheet" href="../assets/css/style.css">
        </head>
        <body>

        <?php include('view/header.php'); ?>

        <div class="sectionInstallation">
        </div>

        <div class="container Presentation">
            <h1 style="color: white; font-size: 30px; margin-bottom: 50px" class="center">Installation</h1>
            <p>Découvrir comment installer votre application.</p>
        </div>


        <div class="container">

            <p>Vous pouvez utiliser l'application sur votre système pour bénéficier d'une expérience Web rapide
                sur votre pc ou sur votre téléphone mobile.</p>

            <p>Vous pouvez installer facilement l'application pour obtenir un accès plus rapide ainsi que des fonctionnalités supplémentaires.</p>

            <div style="height: 15px"></div>

            <h4 style="color: white; font-size: 30px; margin-bottom: 50px" class="center">Choisissez votre système</h4>

            <ul>

                <li>
                    <div class="collapsible-header waves-effect waves-light modal-trigger" href="#ModalAndroid">
                        <img style="width: 170px; margin: 0 auto" src="../assets/img/installation/android.png">
                    </div>
                </li>

                <li>
                    <div class="collapsible-header waves-effect waves-light modal-trigger" href="#ModalWindows">
                        <img style="width: 200px; margin: 0 auto" src="../assets/img/installation/windows.png">
                    </div>
                </li>

                <li>
                    <div class="collapsible-header waves-effect waves-light modal-trigger" href="#ModalIphone">
                        <img style="width: 80px; margin: 0 auto" src="../assets/img/installation/ios.png">
                    </div>
                </li>

            </ul>

            <div style="height: 20px"></div>

            <!-- Modal Android -->
            <div id="ModalAndroid" class="modal">
                <div class="modal-content">
                    <p class="black-text">En cours de réalisation, merci de votre compréhension.</p>
                </div>
            </div>

            <!-- Modal Windows -->
            <div id="ModalWindows" class="modal">
                <div class="modal-content">
                    <p class="black-text">En cours de réalisation, merci de votre compréhension.</p>
                </div>
            </div>

            <!-- Modal IOS -->
            <div id="ModalIphone" class="modal">
                <div class="modal-content">
                    <p class="black-text">En cours de réalisation, merci de votre compréhension.</p>
                </div>
            </div>

        </div>



        <?php include('view/footer.php'); ?>

        </body>
</html>
