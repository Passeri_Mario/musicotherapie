<!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>Musicothérapie</title>
        <meta property="og:title" content="Application PWA Musicothérapie" />
        <meta property="og:url" content="https://musicotherapie.mariopasseri.eu" />
        <meta name="geo.placename" content="Soultz-Sous-Forêts, Alsace">
        <meta name="description" content="Application mobile PWA de Musicothérapie comportant différentes catégories de musiques afin d'accompagner
             l'utilisateur dans son quotidien."/>
        <meta name="author" content="Passeri Mario" />
        <meta name="copyright" content="Passeri Mario" />
        <meta name="geo.country" content="FR">
        <!-- PWA -->
        <link rel="manifest" href="../manifest.json">
        <meta name="theme-color" content="#3c4ca6"/>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../assets/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../assets/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../assets/img/favicon/android-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../assets/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="../assets/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
        <!-- css -->
        <link rel="stylesheet" href="../assets/css/MaterialICO.css" >
        <link rel="stylesheet" href="../assets/css/materialize.css" >
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>

    <body>

    <?php include('view/header.php') ;?>

        <main>

           <div class="Presentation row container">
                    <div class="col s4">
                      <img class="logo-music" src="../assets/img/logo/music-page1.svg" alt="music page1">
                    </div>
                    <div class="col s8">
                        <p>La musicothérapie consiste à utiliser la musique, c'est-à-dire le rythme, la mélodie et l'harmonie,
                           en tant qu'outils thérapeutique</p>
                    </div>
            </div>

            <a href="/douce_nuit">
                <div class="section7 row">
                    <div>
                        <div class="col s4 ajuste">
                            <p><span class="soulignement">N</span>oir</p>
                        </div>
                        <div class="col s8">
                            <p>Musiques<br>Douce Nuit</p>
                        </div>
                    </div>
                </div>
            </a>

            <a href="/relaxante">
                <div class="section6 row">
                    <div>
                        <div class="col s4 ajuste">
                            <p><span class="soulignement">V</span>iolet</p>
                        </div>
                        <div class="col s8">
                            <p>Musiques<br>Relaxante</p>
                       </div>
                    </div>
                </div>
            </a>

            <a href="/nature">
                <div class="section4 row">
                    <div>
                        <div class="col s4 ajuste">
                            <p><span class="soulignement">V</span>ert</p>
                        </div>
                        <div class="col s8">
                            <p>Musiques<br>nature</p>
                        </div>
                        </div>
                   </div>
                </a>

                <a href="/meteorologique">
                    <div class="section2 row">
                        <div>
                            <div class="col s4 ajuste">
                                <p><span class="soulignement">C</span>yan</p>
                            </div>
                            <div class="col s8">
                                <p>Bruits<br>météorologique</p>
                            </div>
                        </div>
                    </div>
                </a>

                <a href="/energisante">
                    <div class="section3 row">
                       <div>
                            <div class="col s4 ajuste">
                                <p><span class="soulignement">J</span>aune</p>
                            </div>
                            <div class="col s8">
                                <p>Musiques<br>Énergisante</p>
                            </div>
                        </div>
                    </div>
                </a>

                <a href="/aventure">
                    <div class="section5 row">
                        <div>
                            <div class="col s4 ajuste">
                                <p><span class="soulignement">B</span>leu</p>
                            </div>
                            <div class="col s8">
                                <p>Musiques<br>Aventure</p>
                            </div>
                        </div>
                    </div>
                </a>
        </main>

        <?php include ('view/footer.php'); ?>

    </body>
</html>
