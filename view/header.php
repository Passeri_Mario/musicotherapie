<header>
    <nav class=" black" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="/" class="brand-logo">Musicothérapie</a>
            <ul class="right hide-on-med-and-down">
                <li><a id="Pseudos" href="/"></a></li>
                <li><a href="/">Menu</a></li>
                <li><a href="/espace">Mon espace</a></li>
            </ul>

            <ul id="nav-mobile" class="sidenav">
                <li class="LogoPseudoMobile"><img class="LogoMobile" src="../assets/img/header/default.png"></li>
                <li class="PseudoMobile"><a style="font-size: 17px" id="PseudoMobile" href="/"></a></li>
                <li>
                    <img src="../assets/img/header/header.jpeg" alt="logo mobile">
                </li>
                <li><a href="/"><i class="material-icons">home</i> Menu</a></li>
                <li><a href="/espace"><i class="material-icons">account_circle</i> Mon espace</a></li>
                <li><a href="/installation"><i class="material-icons">add_circle</i> Installation</a></li>
                <li><div class="divider"></div></li>
                <li><br><a href="https://www.linkedin.com/in/passerimario/"><i class="material-icons">loyalty</i>By Passeri Mario</a></li>
                <li><a href="#!"><i class="material-icons">card_membership</i>1.0.0</a></li>

            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</header>

