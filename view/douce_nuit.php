<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
            <title>Musicothérapie</title>
            <meta property="og:title" content="Application PWA Musicothérapie" />
            <meta property="og:url" content="https://musicotherapie.mariopasseri.eu" />
            <meta name="geo.placename" content="Soultz-Sous-Forêts, Alsace">
            <meta name="description" content="Application mobile PWA de Musicothérapie comportant différentes catégories de musiques afin d'accompagner
                     l'utilisateur dans son quotidien."/>
            <meta name="author" content="Passeri Mario" />
            <meta name="copyright" content="Passeri Mario" />
            <meta name="geo.country" content="FR">
            <!-- PWA -->
            <link rel="manifest" href="../manifest.json">
            <meta name="theme-color" content="#3c4ca6"/>
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="mobile-web-app-capable" content="yes">
            <!-- Favicon -->
            <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="../assets/img/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="../assets/img/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="../assets/img/favicon/android-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="../assets/img/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192" href="../assets/img/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
            <!-- css -->
            <link href="../assets/css/MaterialICO.css" rel="stylesheet">
            <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
            <link href="../assets/css/categories/DouceNuit-style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        </head>

        <body>

        <?php include('view/header.php'); ?>

        <main>
            <div class="Presentation row container">
                <div class="text-doucenuit col s8">
                    <p>Toutes les plus belles musiques pour vous aider a dormir</p>
                </div>

                <div class="col s4">
                    <img class="logo-DouceNuit" src="../assets/img/categories/Douce-Nuit.png" alt="music page1">
                </div>
            </div>

            <div class="section1 row">
                <div>
                    <div class="col s12">
                        <p>Musiques Douce nuit</p>
                    </div>
                </div>
            </div>

           <?php
                // Comptage des musiques disponible
                $dossier='assets/mp3/Douce_nuit/';
                $files = (new RecursiveDirectoryIterator($dossier));


            for($i=1; $i<iterator_count($files)-1; $i++){
                echo '<div class="row center container">
                        <div>
                            <div class="col s1"></div>
                            <div class="col s2">
                                <p class="Vue" style="font-size: 16px; margin-top: 25px"><i class="material-icons">music_note</i></p>
                            </div>
                            <div class="col s5">
                                <p style="font-size: 16px; margin-top: 25px">Douce nuit '.$i.'</p>
                            </div>
                            <div class="col s2">
                                <form method="post" action="/lecteur_douce_nuit">                                   
                                    <input value="doucenuit'.$i.'" name="song" type="hidden">
                                    <button style="background-color:transparent; border: solid transparent"><i class="Player material-icons">play_circle_filled</i></button>
                                </form>
                            </div>
                            <div class="col s1"></div>
                        </div>
                    </div>';
            }

            ?>

        </main>

        <?php include('view/footer.php'); ?>

    </body>

</html>
