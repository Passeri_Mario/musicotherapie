<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col lg12 s12">
                <h5 class="white-text center">Application musicothérapie</h5>
                <p class="grey-text text-lighten-4">Il est strictement interdit de copier et
                    de distribuer le contenu de l'application sans l'accord express des compositeurs.</p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div style="display: flex; justify-content: space-around" class="container">
            <a class="License grey-text text-lighten-4 left" href="/compositeurs">Compositeurs</a>
            <img src="../assets/img/footer/php.png" alt="développeur php passeri mario">
            <a class="License grey-text text-lighten-4 right" href="/installation">Installation</a>
        </div>
    </div>
</footer>

<script>
    if ("serviceWorker" in navigator) {if (navigator.serviceWorker.controller) {console.log("[PWA Builder] travailleur de service actif trouvé, pas besoin de s'inscrire");}
    else {navigator.serviceWorker.register("pwabuilder-sw.js", {scope: "./"}).then(function (reg) {console.log("[PWA Builder] L'agent de service a été enregistré pour l'étendue:" + reg.scope);});}}
</script>
<script defer src="../assets/js/lib-vendor.min.js"></script>
<script src="../pwabuilder-sw.js"></script>
<script defer src="../assets/js/app.js"></script>
