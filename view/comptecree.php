<?php

    $date = date("d-m-Y");
    $heure = date("H:i");

    // Récupération des variables et sécurisation des données

    // Convertion des caractères spéciaux

    $pseudo = htmlentities($_POST['pseudo']);
    $age = htmlentities($_POST['age']);

    $destinataire = 'dev@mariopasseri.eu';

    $sujet = 'Création compte musicotherapie';

    $contenu = '<html lang="fr"><head>
                        <meta charset="utf-8" />
                            <title>Message Visiteur</title>
                        </head>
                        <body style="text-align: center">
                            <p><strong>Pseudo</strong> : '.$pseudo.' </p>
                            <p><strong>Age</strong> : '.$age.' ans </p>                         
                            <p><strong>Compte creé le </strong> : ' . $date .' à '. $heure .' </p>
                        </body>
                    </html>';


    // Email HTML

    $headers = 'From: "Passeri Mario "dev@mariopasseri.eu' . "\r\n";;
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers = 'Content-type: text/html; charset=utf-8' . "\r\n";

    mail($destinataire, $sujet, $contenu, $headers); // Fonction principale qui envoi l'email

?>

<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
            <title>Musicothérapie</title>
        </head>
    <body onload="CompteCree()"></body>
    <script>
        if ("serviceWorker" in navigator) {if (navigator.serviceWorker.controller) {console.log("[PWA Builder] travailleur de service actif trouvé, pas besoin de s'inscrire");}
        else {navigator.serviceWorker.register("pwabuilder-sw.js", {scope: "./"}).then(function (reg) {console.log("[PWA Builder] L'agent de service a été enregistré pour l'étendue:" + reg.scope);});}}
    </script>
    <script defer src="../assets/js/lib-vendor.min.js"></script>
    <script src="../pwabuilder-sw.js"></script>
    <script defer src="../assets/js/app.js"></script>
</html>
