        <!DOCTYPE html>
        <html lang="fr">
        <head>
            <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
            <title>Musicothérapie</title>
            <meta property="og:title" content="Application PWA Musicothérapie" />
            <meta property="og:url" content="https://musicotherapie.mariopasseri.eu" />
            <meta name="geo.placename" content="Soultz-Sous-Forêts, Alsace">
            <meta name="description" content="Application mobile PWA de Musicothérapie comportant différentes catégories de musiques afin d'accompagner
                     l'utilisateur dans son quotidien."/>
            <meta name="author" content="Passeri Mario" />
            <meta name="copyright" content="Passeri Mario" />
            <meta name="geo.country" content="FR">
            <!-- PWA -->
            <link rel="manifest" href="../manifest.json">
            <meta name="theme-color" content="#3c4ca6"/>
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="mobile-web-app-capable" content="yes">
            <!-- Favicon -->
            <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="../assets/img/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="../assets/img/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="../assets/img/favicon/android-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="../assets/img/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192" href="../assets/img/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
            <!-- css -->
            <link rel="stylesheet" href="../assets/css/MaterialICO.css" >
            <link rel="stylesheet" href="../assets/css/materialize.css" >
            <link rel="stylesheet" href="../assets/css/style.css">
        </head>
        <body>

        <?php include('view/header.php'); ?>

        <div class="container Presentation">
            <h1 style="color: white; font-size: 30px; margin-bottom: 50px" class="center">Espace compositeurs</h1>
            <p>Découvrez la liste de tous les compositeurs officiel de l'application musicothérapie.</p>

        </div>

        <div class="container">
            <div class="row">
                <div class="col s2">
                    <p><i style="color: #ff94a3; margin-top: 11px; font-size: 40px" class="material-icons">person</i></p>
                </div>
                <div class="col s1"></div>
                <div class="col s6"><p style="font-size: 16px; margin-top: 35px">En cous de réalisation</p></div>
                <div class="col s1"></div>
                <!-- Modal Trigger -->
                <p><i style="color: #5effb0; margin-top: 21px; font-size: 40px" class="material-icons">copyright</i></p>
            </div>
        </div>

        <?php include('view/footer.php'); ?>

        </body>
</html>
